using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerPegarse : MonoBehaviour
{

    [SerializeField]
    GameObject Mochila;

    [SerializeField]
    GameObject Pokemons;

    [SerializeField]
    GameObject Ataques;

    [SerializeField]
    GameObject Principal;


    [SerializeField]

    GameObject HUD_pegarse;


  


    private void Awake()
    {
        Mochila.SetActive(false);
        Pokemons.SetActive(false);
        Ataques.SetActive(false);


        Principal.SetActive(true);
        HUD_pegarse.SetActive(true);

    }

    private void Start()
    {
        Mochila.SetActive(true);
        Mochila.SetActive(false);
        Pokemons.SetActive(true);
        Pokemons.SetActive(false);
        Ataques.SetActive(true);
        Ataques.SetActive(false);
    
    }


    public void MostrarMochila()
    {
        
        Pokemons.SetActive(false);
        Ataques.SetActive(false);
        Principal.SetActive(false);
        HUD_pegarse.SetActive(false);


        Mochila.SetActive(true);
        
    }
    public void MostrarPokemon()
    {

        Ataques.SetActive(false);
        Mochila.SetActive(false);
        Principal.SetActive(false);
        HUD_pegarse.SetActive(false);


        Pokemons.SetActive(true);
       
    }

    public void MostrarAtaques()
    {
        Principal.SetActive(false);
        Pokemons.SetActive(false);
        Mochila.SetActive(false);


        HUD_pegarse.SetActive(true);
        Ataques.SetActive(true);

        
        
    }


    public void Atras()
    {
        
        Ataques.SetActive(false);
        Pokemons.SetActive(false);
        Mochila.SetActive(false);


        HUD_pegarse.SetActive(true);
        Principal.SetActive(true);
    }



    public void Huida()
    {
        // literalmente calculos que ns si existen porque habeces no te dejaba uir en el juego

        SceneManager.LoadScene("Gerard");
    }

    private void OnLevelWasLoaded(int level)
    {
        
    }


}
