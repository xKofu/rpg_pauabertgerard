using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCalc : MonoBehaviour
{

    [SerializeField]
    private ScriptableMokepon Atacante;

    [SerializeField]
    private ScriptableMokepon Defensor;

    [SerializeField]
    private Movimiento Mov;


    public void Prueba()
    {
        Atacar(Atacante, Defensor, Mov);
    }

    public void Atacar(ScriptableMokepon Atacante, ScriptableMokepon Defensor, Movimiento Ataque)
    {
        print(Defensor.hpActual);
        float da�o = ((((((2 * Atacante.nivel) / 5) + 2) * Ataque.poder * (Atacante.atk / Defensor.def)) / 50) + 2)
            * (this.CalcularEfectividad(Ataque.tipo, Defensor.tipoActividadSesua) * this.CalcularEfectividad(Ataque.tipo, Defensor.tipoSexualidade));

        Defensor.hpActual = (int)(Defensor.hpActual - da�o);

        print(Defensor.hpActual);

    }

    public float CalcularEfectividad(Tipo TipoAtacante, Tipo TipoDefensor)
    {

        if (TipoAtacante == Tipo.HETERO && TipoDefensor == Tipo.CURIOSO
         || TipoAtacante == Tipo.CURIOSO && TipoDefensor == Tipo.SARASA
         || TipoAtacante == Tipo.SARASA && TipoDefensor == Tipo.HETERO
         || TipoAtacante == Tipo.LIMPIO && TipoDefensor == Tipo.HORNEADO
         || TipoAtacante == Tipo.HORNEADO && TipoDefensor == Tipo.ZARRAPASTROSO
         || TipoAtacante == Tipo.ZARRAPASTROSO && TipoDefensor == Tipo.LIMPIO)
        {
            return 2f;
        }
        else if (TipoAtacante == Tipo.HETERO && TipoDefensor == Tipo.SARASA
              || TipoAtacante == Tipo.CURIOSO && TipoDefensor == Tipo.HETERO
              || TipoAtacante == Tipo.SARASA && TipoDefensor == Tipo.CURIOSO
              || TipoAtacante == Tipo.LIMPIO && TipoDefensor == Tipo.ZARRAPASTROSO
              || TipoAtacante == Tipo.HORNEADO && TipoDefensor == Tipo.LIMPIO
              || TipoAtacante == Tipo.ZARRAPASTROSO && TipoDefensor == Tipo.HORNEADO)
        {
            return 0.5f;
        }
        else
        {
            return 1f;
        }

    }

}
