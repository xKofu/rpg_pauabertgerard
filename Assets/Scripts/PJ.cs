using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PJ : MonoBehaviour
{

    [SerializeField]
    public int nMokepons;
    [SerializeField]
    private bool vivo;
    [SerializeField]
    public List<ScriptableMokepon> equipo;

    [SerializeField]
    public List<Equipo> inventario;


    [SerializeField]
    private GameObject Target;

    [SerializeField]
    public int battleRate = 3;

    [SerializeField]
    private GameEvent Combatir;

    [SerializeField]
    private GameEvent Cueva;

    [SerializeField]
    private GameEvent Bosque;

    private Vector3 PosAnterior;


    [SerializeField]
    private Pokedex pokemonsDisponibles;

    [SerializeField]
    private CompendioObjetos objetosDisponibles;

    [SerializeField]
    private ScriptableMokepon enemigoCombate;


    private bool catcheado = false;
    

    void Start()
    {
        PosAnterior = new Vector3();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ImprimirEquipo();
        }
        ComprovarMuerto();

        AnimacionMovimiento();

        transform.position = new Vector3(transform.position.x, transform.position.y, 0);

    }

    void ComprovarMuerto()
    {
        nMokepons = 0;
        for(int i = 0; i < equipo.Count; i++)
        {
            if(equipo[i] != null)
            {
                nMokepons++;
            }
        }

        if(this.nMokepons <= 0)
        {
            this.vivo = false;

        }
    }

    void ImprimirEquipo()
    {
        foreach(ScriptableMokepon sm in this.equipo){
            Debug.Log(sm.nom+" "+sm.tipoSexualidade+" " + sm.tipoActividadSesua + " " + sm.equipo.nombre);
            foreach(Movimiento mov in sm.movimientos)
            {
                Debug.Log(mov.nombre);
            }
            
        }
    }

    void AnimacionMovimiento()
    {
        float directionChangeDelta = 2 * Time.deltaTime; // Este 2 es de AiLerp que no se puede pillar

        if ((transform.position.y - PosAnterior.y) > directionChangeDelta)
        {
            GetComponent<Animator>().Play("WalkAnimationU");
            //print("ARRIBA");
        }else if ((PosAnterior.y- transform.position.y) > directionChangeDelta)
        {
            GetComponent<Animator>().Play("WalkAnimationD");
            //print("ABAJO");
        }else if ((transform.position.x - PosAnterior.x) > directionChangeDelta)
        {
            GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<Animator>().Play("WalkAnimationH");
            //print("Derecha");
        }
        else if ((PosAnterior.x - transform.position.x) > directionChangeDelta)
        {
            GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<Animator>().Play("WalkAnimationH");
            //print("Izquierda");
        }

        if (transform.position == PosAnterior)
        {
            GetComponent<Animator>().Play("IdleAnimation");
            //print("quieto");
        }
        //print((transform.position.x - PosAnterior.x) + " -> " + (transform.position.y - PosAnterior.y));
        PosAnterior = transform.position;

    }

    private void PillarMokepon(GameObject pillao)
    {
        bool yalotengo = false;
        if(nMokepons < 6)
        {
            for(int i = 0; i < nMokepons; i++)
            {
                if(pillao.name == equipo[i].nom)
                {
                    yalotengo = true;
                    print("no porque ya te tengo");
                }
            }

            if (!yalotengo)
            {
                foreach(ScriptableMokepon sm in pokemonsDisponibles.pokedex)
                {
                    if(sm.nom == pillao.name)
                    {
                        print("te capturo");
                        equipo[nMokepons] = sm;
                        catcheado = true;
                        return;
                    }
                }
                
            }
        }
    }

    private void PillarObjeto(GameObject pilladete)
    {
        foreach(Equipo e in objetosDisponibles.compendio)
        {
            if(e.nombre == pilladete.name)
            {
                inventario.Add(e);
                return;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "grass")
        {
            print("colision");
            int aux = Random.Range(1, 10);
            if (aux < battleRate)
            {
                print("combate");
                int rango = pokemonsDisponibles.pokedex.Count;
                enemigoCombate = pokemonsDisponibles.pokedex[Random.Range(0, rango)];
                GeneralManager.Instance.setEnemigo(enemigoCombate);
                Combatir.Raise();
            }
        }

        if (collision.tag == "bossfinal")
        {
            print("combateFinal");
            GeneralManager.Instance.setEnemigo(enemigoCombate);
            Combatir.Raise();
        }

        if (collision.tag == "tpcueva")
        {
            print("safdasf");
            Cueva.Raise();
        }

        if (collision.tag == "tpbosque")
        {
            print("safdasf");
            Bosque.Raise();
        }

        if (collision.tag == "pomeko")
        {
            print("te capturo?");
            PillarMokepon(collision.gameObject);
            if (catcheado)
            {
                Destroy(collision.gameObject);
            }
        }

        if (collision.tag == "objeto")
        {
            print("de locos me he encontrado un " + collision.name+" en el suelo");
            PillarObjeto(collision.gameObject);
            Destroy(collision.gameObject);
        }


    }

}
