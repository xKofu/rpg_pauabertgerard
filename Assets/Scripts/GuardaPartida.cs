using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Save", menuName = "ScriptableObjects/Save")]
[Serializable]
public class GuardaPartida : ScriptableObject
{
    public Vector3 posPJ;
    public int[] equipoPoguemons;
    public string[] inventario;
    public int[] cantidadObjeto;
    public string nombreEscena;
}
