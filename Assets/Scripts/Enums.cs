public enum BuffoStat
{
    ATK, DEF, VEL, ATKSP, DEFSP, HP
}

public enum Tipo
{
    HETERO, HORNEADO, SARASA, CURIOSO, ZARRAPASTROSO, LIMPIO
}

//Los pomekos tendran 2 tipos, uno en el que sea Hetero/Sarasa/Curioso y otro entre Horneado/Zarrapastroso/Limpio

//TABLA DE TIPOS -> efectividades (debilidades recíprocas)
/*
 * Hetero: x2 a curioso, x0.5 a sarasa
 * Sarasa: x2 a hetero, x0.5 a curioso
 * Curioso: x2 a sarasa, x0.5 a hetero
 * Horneado: x2 a zarrapastroso, x0.5 a limpio
 * Zarrapastroso: x2 a limpio, x0.5 a horneado
 * Limpio: x2 a horneado, x0.5 a zarrapastroso
 */
