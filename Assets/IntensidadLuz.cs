using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class IntensidadLuz : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Intensidad());
    }

    
    IEnumerator Intensidad()
    {
        bool flag = true;

        while(true)
        {
            
            if (GetComponent<Light2D>().intensity > 1.50f)
                flag = true;

            if (GetComponent<Light2D>().intensity < 1)
                flag = false;

            if (flag)
            {
                GetComponent<Light2D>().intensity -= 0.09f;
            } else
            {
                GetComponent<Light2D>().intensity += 0.09f;
            }
            
            yield return new WaitForSeconds(0.15f);


        }

    }

}
