using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GeneralManager : MonoBehaviour
{

    [SerializeField]
    private GameEvent SceneLoaded;

    [SerializeField]
    private GameEvent Combate;

    [SerializeField]
    private GameEvent Bosque;

    [SerializeField]
    private GameEvent Cueva;

    [SerializeField]
    PJ pj;


    List<ScriptableMokepon> pokemons_disponibles_jugador = new List<ScriptableMokepon>();
    List<Equipo> inventarioJUgador = new List<Equipo>();

    [SerializeField]
    private GuardaPartida partida;

    [SerializeField]
    public GuardaPartida estadoAlCambiarEscena;

    [SerializeField]
    private Pokedex pokedex;

    [SerializeField]
    private CompendioObjetos compendio;

    private LevelLoaderManager LV;        

    [SerializeField]
    private ScriptableMokepon enemigo;

    private static GeneralManager m_instance = null;
    public static GeneralManager Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType<GeneralManager>();

            return m_instance;
        }
    }

    private void Awake()
    {
        pj = FindObjectOfType<PJ>();
        pokemons_disponibles_jugador = pj.equipo;
        inventarioJUgador = pj.inventario;

    }


    void Start()
    {
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

  

    public void Save()
    {
        print(pj);
        partida.posPJ = this.transform.position;
        
        for (int i = 0; i < pj.nMokepons; i++)
        {
            print(i + " " + pj.equipo[i].nom);
            partida.equipoPoguemons[i] = pj.equipo[i].id;
        }
        print(pj.inventario.Count);
        for (int i = 0; i < pj.inventario.Count; i++)
        {
            partida.inventario[i] = pj.inventario[i].nombre;
            partida.cantidadObjeto[i] = pj.inventario[i].cantidad;
        }
        partida.nombreEscena = SceneManager.GetActiveScene().name;
        string jsonStr = JsonUtility.ToJson(partida);
        print(jsonStr);
        File.WriteAllText("savegame.json", jsonStr);
        print("Game Saved");
    }

    public void Load()
    {

        string jsonStr = File.ReadAllText("savegame.json");

        JsonUtility.FromJsonOverwrite(jsonStr, partida);

        SceneManager.LoadScene(partida.nombreEscena);

        pj.transform.position = partida.posPJ;

        while (pj.equipo.Count > 0)
        {
            pj.equipo.RemoveAt(0);
        }

        while (pj.inventario.Count > 0)
        {
            pj.inventario.RemoveAt(0);
        }

        for (int i = 0; i < partida.equipoPoguemons.Length; i++)
        {
            for (int j = 0; j < pokedex.pokedex.Count; j++)
            {
                if (partida.equipoPoguemons[i] == pokedex.pokedex[j].id)
                {
                    pj.equipo.Add(pokedex.pokedex[j]);
                }
            }
        }

        pj.nMokepons = pj.equipo.Count;

        while (pj.equipo.Count < 6)
        {
            pj.equipo.Add(null);
        }

        for (int i = 0; i < partida.inventario.Length; i++)
        {
            for (int j = 0; j < compendio.compendio.Count; j++)
            {
                if (partida.inventario[i] == compendio.compendio[j].nombre)
                {
                    pj.inventario.Add(compendio.compendio[j]);
                    pj.inventario[pj.inventario.Count - 1].cantidad = partida.cantidadObjeto[i];
                }
            }
        }

        print("Game Loaded");
    }

    public void OnEntrarCombate()
    {
        
        estadoAlCambiarEscena.nombreEscena = SceneManager.GetActiveScene().name;

        pj = FindObjectOfType<PJ>();

        estadoAlCambiarEscena.posPJ = pj.transform.position;
        print("maricon");
        for (int i = 0; i < pj.nMokepons; i++)
        {
            print("super maricon");
            estadoAlCambiarEscena.equipoPoguemons[i] = pj.equipo[i].id;
        }
        print(pj.inventario.Count);
        for (int i = 0; i < pj.inventario.Count; i++)
        {
            estadoAlCambiarEscena.inventario[i] = pj.inventario[i].nombre;
            estadoAlCambiarEscena.cantidadObjeto[i] = pj.inventario[i].cantidad;
        }
        
    }

    IEnumerator CooldownBattle()
    {
        pj.battleRate = 0;
        print("desactivao");
        yield return new WaitForSeconds(2);
        pj.battleRate = 3; //valor original de battlerate
        print("activao");
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        print(scene.name);
        if (scene.name == "GameOver")
        {
            return;
        }

        print(estadoAlCambiarEscena.nombreEscena);

        if (estadoAlCambiarEscena.nombreEscena == "EscenaCombate")
        {
            print("he entrao");

            pj = FindObjectOfType<PJ>();

            StartCoroutine("CooldownBattle"); //corrutina para que no te salga un combate nada m�s volver a la escena

            pj.transform.position = estadoAlCambiarEscena.posPJ;
         

            while (pj.equipo.Count > 0)
            {
                pj.equipo.RemoveAt(0);
            }

            while (pj.inventario.Count > 0)
            {
                pj.inventario.RemoveAt(0);
            }

            for (int i = 0; i < estadoAlCambiarEscena.equipoPoguemons.Length; i++)
            {
                for (int j = 0; j < pokedex.pokedex.Count; j++)
                {
                    if (estadoAlCambiarEscena.equipoPoguemons[i] == pokedex.pokedex[j].id)
                    {
                        pj.equipo.Add(pokedex.pokedex[j]);
                    }
                }
            }

            pj.nMokepons = pj.equipo.Count;

            while (pj.equipo.Count < 6)
            {
                pj.equipo.Add(null);
            }

            for (int i = 0; i < estadoAlCambiarEscena.inventario.Length; i++)
            {
                for (int j = 0; j < compendio.compendio.Count; j++)
                {
                    if (estadoAlCambiarEscena.inventario[i] == compendio.compendio[j].nombre)
                    {
                        pj.inventario.Add(compendio.compendio[j]);
                        pj.inventario[pj.inventario.Count - 1].cantidad = estadoAlCambiarEscena.cantidadObjeto[i];
                    }
                }
            }

        }

        if (estadoAlCambiarEscena.nombreEscena == "Cueva")
        {
            pj = FindObjectOfType<PJ>();

            StartCoroutine("CooldownBattle"); //corrutina para que no te salga un combate nada m�s volver a la escena

            pj.transform.position = GameObject.Find("CambioCueva").transform.position;
        
        }

        if (estadoAlCambiarEscena.nombreEscena == "Gerard")
        {
            pj = FindObjectOfType<PJ>();

            StartCoroutine("CooldownBattle"); //corrutina para que no te salga un combate nada m�s volver a la escena

            pj.transform.position = GameObject.Find("Bosque").transform.position;

        }

        SceneLoaded.Raise();
    }

    public void CambioParametroLM(int a)
    {
        LV = FindObjectOfType<LevelLoaderManager>();
        estadoAlCambiarEscena.nombreEscena = SceneManager.GetActiveScene().name;

        if (a == 0)
        {
            LV.LoadNextLevel("EscenaCombate");
            //Combate    
        }
        else if (a == 1)
        {
            LV.LoadNextLevel("Gerard");
            //Bosque
        } else
        {
            LV.LoadNextLevel("Cueva");
            //Cueva
        }
        
    }

    public List<ScriptableMokepon> getList()
    {
        return pokemons_disponibles_jugador;
    }

    public void setEnemigo(ScriptableMokepon e)
    {
        enemigo = e;
    }

    public ScriptableMokepon getEnemigo()
    {
        return enemigo;
    }

    public List<Equipo> inventario()
    {
        return inventarioJUgador;
    }
}
