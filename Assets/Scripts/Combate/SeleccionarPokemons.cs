using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeleccionarPokemons : MonoBehaviour
{
    [SerializeField]
    List<GameObject> pokemons_disponibles = new List<GameObject>();

    [SerializeField]
    List<ScriptableMokepon> pokemons_disponibles_so = new List<ScriptableMokepon>();


    private int ID;

    [SerializeField]
    private GameObject selector;
    [SerializeField]
    GameObject Opciones;


    [SerializeField]
    GameObject padre;



    [SerializeField]
    GameObject invetario;
    [SerializeField]
    GameObject Mochila;

    void Start()
    {
        
    }

    private void Awake()
    {
        putInfo();
        
    }
    public void putInfo()
    {
        pokemons_disponibles_so = GeneralManager.Instance.getList();

        for (int i = 0; i < pokemons_disponibles_so.Count; i++)
        {
            if (pokemons_disponibles_so[i])
            {
                if (pokemons_disponibles_so[i].equipo)
                {
                    /*
                    if (pokemons_disponibles_so[i].equipo.statSumada == BuffoStat.ATK)
                    {
                        pokemons_disponibles_so[i].atk = pokemons_disponibles_so[i].atk + pokemons_disponibles_so[i].equipo.sumaStat;
                    }
                    else if (pokemons_disponibles_so[i].equipo.statSumada == BuffoStat.ATKSP)
                    {
                        pokemons_disponibles_so[i].atk = pokemons_disponibles_so[i].atk + (pokemons_disponibles_so[i].equipo.sumaStat * 2);
                    }
                    else if (pokemons_disponibles_so[i].equipo.statSumada == BuffoStat.DEF)
                    {
                        pokemons_disponibles_so[i].def = pokemons_disponibles_so[i].def + pokemons_disponibles_so[i].equipo.sumaStat;
                    }
                    else if (pokemons_disponibles_so[i].equipo.statSumada == BuffoStat.DEFSP)
                    {
                        pokemons_disponibles_so[i].def = pokemons_disponibles_so[i].def + (pokemons_disponibles_so[i].equipo.sumaStat * 2);
                    }
                    else if (pokemons_disponibles_so[i].equipo.statSumada == BuffoStat.VEL)
                    {
                        pokemons_disponibles_so[i].vel = pokemons_disponibles_so[i].vel + pokemons_disponibles_so[i].equipo.sumaStat;
                    }
                    */
                }
            }
        }


        for (int i = 0; i< GeneralManager.Instance.getList().Count; i++)
        {

            pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().setInfo(pokemons_disponibles_so[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Surfear();
    }


    private bool PoderMoverse = true;
    public void Surfear()
    {

        if (PoderMoverse)
        {
            if (Input.GetKeyDown(KeyCode.D) && ID < pokemons_disponibles.Count - 1)
            {
                ID++;
            }
            if (Input.GetKeyDown(KeyCode.A) && ID > 0)
            {
                ID--;
            }

            if (Input.GetKeyDown(KeyCode.W) && ID > 1)
            {
                ID -= 2;
            }
            if (Input.GetKeyDown(KeyCode.S) && ID < 4)
            {
                ID += 2;
            }

            selector.transform.position = pokemons_disponibles[ID].transform.position;
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (PoderMoverse)
            {
                PoderMoverse = false;
                Opciones.SetActive(true);
                Opciones.transform.position = pokemons_disponibles[ID].transform.position;
            }
        }
    }

    public void QuitarOpciones()
    {
        Opciones.SetActive(false);
        PoderMoverse = true;
    }





    public void quitarObjecto()
    {

        if (pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo() != null)
        {
            if (pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().equipo != null)
            {
                Equipo objecto = pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().equipo;
                pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().equipo = null;
                

                invetario.GetComponent<Mochila>().añadirObjectoInventario(objecto);

                /*
                if (objecto.statSumada == BuffoStat.ATK)
                {
                    pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().atk = pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().atk - objecto.sumaStat;
                }
                else if (objecto.statSumada == BuffoStat.ATKSP)
                {
                    pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().atk = pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().atk - (objecto.sumaStat * 2);
                }
                else if (objecto.statSumada == BuffoStat.DEF)
                {
                    pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().def = pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().def - objecto.sumaStat;
                }
                else if (objecto.statSumada == BuffoStat.DEFSP)
                {
                    pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().def = pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().def - (objecto.sumaStat * 2);
                }
                else if (objecto.statSumada == BuffoStat.VEL)
                {
                    pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().vel = pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().vel - objecto.sumaStat;
                }
                */
                pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().loadInfo();
            }

        }
       
    }

    public void ponerObjecto()
    {
        print("me dispongo a poner las opciones");

        if (pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo().equipo == null)
        {
            print("entro en la mochila");

            

            invetario.GetComponent<Mochila>().SetPokemonAfectado(pokemons_disponibles[ID].GetComponent<LoadInfo_Pokemons>().getInfo());
        }
        else
        {

            print("el pokemon ya tiene un objeto");
        }
    }




    public void RecargarInformacion()
    {
        for (int i = 0; i < pokemons_disponibles.Count; i++)
        {
            pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().loadInfo();
        }
    }


    public void SacarPokemon()
    {
        // todos a falso
        for(int i = 0;i< pokemons_disponibles.Count;i++)
        {
            if (pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().getInfo())
            {
                pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().getInfo().combatiendo = false;
            }
            
        }


        print(pokemons_disponibles_so[1]);

        for (int i = 0; i < pokemons_disponibles_so.Count; i++)
        {
            if (pokemons_disponibles_so[i])
            {
                pokemons_disponibles_so[i].combatiendo = false;
            }
        }
        // pongo el que toca a true
        pokemons_disponibles_so[ID].combatiendo = true;
    }
}
