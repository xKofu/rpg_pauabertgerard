using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Pokedex", menuName = "ScriptableObjects/Pokedex")]
public class Pokedex : ScriptableObject
{
    public List<ScriptableMokepon> pokedex;
  

}
