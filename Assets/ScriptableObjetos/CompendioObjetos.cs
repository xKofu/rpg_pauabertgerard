using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Compendio", menuName = "ScriptableObjects/Compendio")]
public class CompendioObjetos : ScriptableObject
{
    public List<Equipo> compendio;


}