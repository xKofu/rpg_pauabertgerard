using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GMLoader : MonoBehaviour
{
    [SerializeField]
    private GameObject gameManager;
   
    void Awake()
    {
        GeneralManager gm = GameObject.FindObjectOfType<GeneralManager>();
        if(gm == null)
        {
            print("GameManager created.");
            Instantiate(gameManager);
        }
    }

    void ChangeLevel(string level)
    {
        
    }

}
