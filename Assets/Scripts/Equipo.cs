using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Equipo", menuName = "ScriptableObjects/Equipo")]
[Serializable]
public class Equipo : ScriptableObject
{
    public string nombre;
    public int cantidad;
    public int funcion; //0 cura, 1 suma stats
    public int curacion;
    public BuffoStat statSumada;
    public int sumaStat;
    public Sprite foto;
}
