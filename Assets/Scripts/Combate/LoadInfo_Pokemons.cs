using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class LoadInfo_Pokemons : MonoBehaviour
{


    [SerializeField]
    ScriptableMokepon info;


    [SerializeField]
    GameObject vida;

    [SerializeField]
    GameObject nombre;

    [SerializeField]
    GameObject foto;

    [SerializeField]
    GameObject objeto;


    [SerializeField]
    GameObject BarraDeVida;



    void Start()
    {
        
        if (info)
        {
            loadInfo();
            BarraDeVida.SetActive(true);
            foto.SetActive(true);
        }

    }


    // slo tendriamos que pasarle el escriptable del que toca y ya
    public void loadInfo()
    {
        if (info)
        {
            vida.GetComponent<TMPro.TextMeshProUGUI>().text = "" + info.hpActual + " / " + info.hpMax;
            nombre.GetComponent<TMPro.TextMeshProUGUI>().text = info.name;
            foto.GetComponent<Image>().sprite = info.foto;
            if (info.equipo == null)
            {
                objeto.SetActive(false);
            }
            else
            {
                objeto.SetActive(true);
                objeto.GetComponent<Image>().sprite = info.equipo.foto;
            }
        }
    }

    public ScriptableMokepon getInfo()
    {
        return info;
    }

    public void setInfo(ScriptableMokepon ScriPokemon)
    {
        info = ScriPokemon;
    }

}
