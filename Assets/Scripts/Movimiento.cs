using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Movimiento", menuName = "ScriptableObjects/Movimiento")]
[Serializable]
public class Movimiento : ScriptableObject
{
    public string nombre;
    public float poder;
    public Tipo tipo;
    public int usosTotales;
    public int usos;

}
