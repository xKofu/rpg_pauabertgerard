using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Turnos : MonoBehaviour
{

    /*
    [SerializeField]
    int enemyN = 0, m_pokemonsN = 0; // si es 0 es que hay 1 (viene mejor por tema listas y tal para lo del party y tal)

    [SerializeField]
    int enemySelected, m_pokemonSelected;
    */
    

    [SerializeField]
    GameManagerPegarse GMP;

    [SerializeField]
    ScriptableMokepon enemy;

    [SerializeField]
    ScriptableMokepon m_pokemon;

    [SerializeField]
    private GameEvent CambiarEscena;

    bool turno = true; // true mi turno, false el suyo



    [SerializeField]
    private bool HeClicadoEnBoton = false;

    

    [SerializeField]
    Ataques_Controller AC;

    [SerializeField]
    private List<Movimiento> ataques = new List<Movimiento>();


    [SerializeField]
    GameObject yo;


    [SerializeField]
    GameEvent pokemonVida, enemigoVida;




    private static Turnos m_instance = null;
    public static Turnos Instance
    {
        get
        {
            if (m_instance == null)
                m_instance = FindObjectOfType<Turnos>();

            return m_instance;
        }
    }

    [SerializeField]
    private List<ScriptableMokepon> m_listaPokemons;

    private void Awake()
    {
        //enemy = GeneralManager.Instance.getEnemigo();
        LoadInfo();
        m_listaPokemons = GeneralManager.Instance.getList();
    }



    public void LoadInfo()
    {


        enemy.id = GeneralManager.Instance.getEnemigo().id;
        
        enemy.nom = GeneralManager.Instance.getEnemigo().nom;
        enemy.movimientos = GeneralManager.Instance.getEnemigo().movimientos;
        enemy.hpActual = GeneralManager.Instance.getEnemigo().hpMax;
        enemy.hpMax = GeneralManager.Instance.getEnemigo().hpMax;
        enemy.atk = GeneralManager.Instance.getEnemigo().atk;
        enemy.def = GeneralManager.Instance.getEnemigo().def;
        enemy.vel = GeneralManager.Instance.getEnemigo().vel;
        enemy.nivel = GeneralManager.Instance.getEnemigo().nivel;
        enemy.equipo = GeneralManager.Instance.getEnemigo().equipo;
        enemy.tipoSexualidade = GeneralManager.Instance.getEnemigo().tipoSexualidade;
        enemy.tipoActividadSesua = GeneralManager.Instance.getEnemigo().tipoActividadSesua;
        enemy.vivo = true;
        enemy.foto = GeneralManager.Instance.getEnemigo().foto;
        enemy.combatiendo = false;

    }









    // mirar donde pongo lo de que me he muerto, seguramente en cuanto ataque el enemigo y sacar la escena de los pokemons,
    // si todos estan fuera que te lleve a nosedonde

    void Update()
    {
        /*
        if (enemyN >= 0 && !turno)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
                enemySelected--;

            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.D))
                enemySelected++;

            enemySelected = Mathf.Clamp(enemySelected, 0, enemyN); //asi el enemySelected no se sale de los limites // fijatetuquecosas
        }
        */

        if (!HeClicadoEnBoton && !turno)
        {
            AtaqueEnemigo();
        }
    }
    

    public void AtaqueEnemigo()
    {
        if (!turno)
        {
            int nAtaques = 0;


            for (int i = 0; i<enemy.movimientos.Count; i++)
            {
                if (enemy.movimientos[i])
                {
                    nAtaques++;
                }
            }
            if (nAtaques>0)
            {
                nAtaques--;
            }

            Movimiento ataqueSeleccionado = enemy.movimientos[Random.Range(0,nAtaques)]; //saco el ataque de la lista

            // iEnumerator para hacer que no nos de verguneza poner nuestro nombre

            Atacar(enemy, m_pokemon, ataqueSeleccionado);

           
            turno = true;
        }
    }




    // puedo ahcer un switch y tal y si a atacado mirar si no tiene vida el enemigo
    public void AtaqueUno()
    {
        if (ataques[0])
        {
            if (!HeClicadoEnBoton) {
                ataques[0].usos--;
                HeClicadoEnBoton = true;
                Atacar(m_pokemon, enemy, ataques[0]);
                
                turno = false;
                AtaqueEnemigo();

                AC.loadInfoAtaques();
            }
        }
        else
        {
            print("no hay ningun ataque hay (boton 1)");
        }
    }
    public void AtaqueDos()
    {
        if (ataques[1])
        {

            if (!HeClicadoEnBoton)
            {
                ataques[1].usos--;
                HeClicadoEnBoton = true;
                Atacar(m_pokemon, enemy, ataques[1]);
                
                turno = false;
                AtaqueEnemigo();

                AC.loadInfoAtaques();
            }
        }
        else
        {
            print("no hay ningun ataque hay (boton 2)");
        }
    }
    public void AtaqueTres()
    {
        if (ataques[2])
        {
            if (!HeClicadoEnBoton)
            {
                ataques[2].usos--;
                HeClicadoEnBoton = true;
                Atacar(m_pokemon, enemy, ataques[2]);
                
                turno = false;
                AtaqueEnemigo();

                AC.loadInfoAtaques();
            }
        }
        else
        {
            print("no hay ningun ataque hay (boton 3)");
        }
    }
    public void AtaqueCuatro()
    {
        if (ataques[3])
        {
            if (!HeClicadoEnBoton)
            {
                ataques[3].usos--;
                HeClicadoEnBoton = true;
                Atacar(m_pokemon, enemy, ataques[3]);
                turno = false;
                AtaqueEnemigo();

                AC.loadInfoAtaques();
            }
        }
        else
        {
            print("no hay ningun ataque hay (boton 4)");
        }
    }






    public void Atacar(ScriptableMokepon Atacante, ScriptableMokepon Defensor, Movimiento Ataque)
    {


        if (turno)
        {
            StartCoroutine(AnimAtaque());
        }



        if (Atacante.equipo)
        {
            if (Atacante.equipo.statSumada == BuffoStat.ATK)
            {
                Atacante.atk = Atacante.atk + Atacante.equipo.sumaStat;
            }
            else if (Atacante.equipo.statSumada == BuffoStat.ATKSP)
            {
                print("supolla");
                Atacante.atk = Atacante.atk + (Atacante.equipo.sumaStat * 2);
            }
            else if (Atacante.equipo.statSumada == BuffoStat.DEF)
            {
                Atacante.def = Atacante.def + Atacante.equipo.sumaStat;
            }
            else if (Atacante.equipo.statSumada == BuffoStat.DEFSP)
            {
                Atacante.def = Atacante.def + (Atacante.equipo.sumaStat * 2);
            }
            else if (Atacante.equipo.statSumada == BuffoStat.VEL)
            {
                Atacante.vel = Atacante.vel + Atacante.equipo.sumaStat;
            }
        }
        if (Defensor.equipo)
        {
            if (Defensor.equipo.statSumada == BuffoStat.ATK)
            {
                Defensor.atk = Defensor.atk + Defensor.equipo.sumaStat;
            }
            else if (Defensor.equipo.statSumada == BuffoStat.ATKSP)
            {
                Defensor.atk = Defensor.atk + (Defensor.equipo.sumaStat * 2);
            }
            else if (Defensor.equipo.statSumada == BuffoStat.DEF)
            {
                Defensor.def = Defensor.def + Defensor.equipo.sumaStat;
            }
            else if (Defensor.equipo.statSumada == BuffoStat.DEFSP)
            {
                Defensor.def = Defensor.def + (Defensor.equipo.sumaStat * 2);
            }
            else if (Defensor.equipo.statSumada == BuffoStat.VEL)
            {
                Defensor.vel = Defensor.vel + Defensor.equipo.sumaStat;
            }
        }


        float da�o = ((((((2 * Atacante.nivel) / 5) + 2) *  Ataque.poder * (Atacante.atk / Defensor.def)) / 50) + 2) * 
            (this.CalcularEfectividad(Ataque.tipo, Defensor.tipoActividadSesua) * this.CalcularEfectividad(Ataque.tipo, Defensor.tipoSexualidade));



        Defensor.hpActual = (int)(Defensor.hpActual - da�o);

        pokemonVida.Raise();
        enemigoVida.Raise();


        //quitar los stats y tal

        if (Atacante.equipo)
        {
            if (Atacante.equipo.statSumada == BuffoStat.ATK)
            {
                Atacante.atk = Atacante.atk - Atacante.equipo.sumaStat;
            }
            else if (Atacante.equipo.statSumada == BuffoStat.ATKSP)
            {
                Atacante.atk = Atacante.atk - (Atacante.equipo.sumaStat * 2);
            }
            else if (Atacante.equipo.statSumada == BuffoStat.DEF)
            {
                Atacante.def = Atacante.def - Atacante.equipo.sumaStat;
            }
            else if (Atacante.equipo.statSumada == BuffoStat.DEFSP)
            {
                Atacante.def = Atacante.def - (Atacante.equipo.sumaStat * 2);
            }
            else if (Atacante.equipo.statSumada == BuffoStat.VEL)
            {
                Atacante.vel = Atacante.vel - Atacante.equipo.sumaStat;
            }
        }
        if (Defensor.equipo)
        {
            if (Defensor.equipo.statSumada == BuffoStat.ATK)
            {
                Defensor.atk = Defensor.atk - Defensor.equipo.sumaStat;
            }
            else if (Defensor.equipo.statSumada == BuffoStat.ATKSP)
            {
                Defensor.atk = Defensor.atk - (Defensor.equipo.sumaStat * 2);
            }
            else if (Defensor.equipo.statSumada == BuffoStat.DEF)
            {
                Defensor.def = Defensor.def - Defensor.equipo.sumaStat;
            }
            else if (Defensor.equipo.statSumada == BuffoStat.DEFSP)
            {
                Defensor.def = Defensor.def - (Defensor.equipo.sumaStat * 2);
            }
            else if (Defensor.equipo.statSumada == BuffoStat.VEL)
            {
                Defensor.vel = Defensor.vel - Defensor.equipo.sumaStat;
            }
        }


        if (Defensor == enemy && Defensor.hpActual <= 0)
        {
            //CambiarEscena.Raise();
            if (GeneralManager.Instance.estadoAlCambiarEscena.nombreEscena == "Cueva")
            {
                GeneralManager.Instance.CambioParametroLM(2);
            }

            if (GeneralManager.Instance.estadoAlCambiarEscena.nombreEscena == "Gerard")
            {
                GeneralManager.Instance.CambioParametroLM(1);
            }

            SceneManager.LoadScene(GeneralManager.Instance.estadoAlCambiarEscena.nombreEscena);
        }

        int contadorMuertos = 0;
        int ContadorExistentes = 0;
        if (Defensor == m_pokemon && Defensor.hpActual <= 0)
        {
            for (int i = 0; i< m_listaPokemons.Count;i++)
            {
                if (m_listaPokemons[i])
                {
                    ContadorExistentes++;
                    if (m_listaPokemons[i].hpActual <= 0)
                    {
                        contadorMuertos++;
                    }
                }
            }

            if (contadorMuertos == ContadorExistentes)
            {

                print("mi equipo ha muerto");
                SceneManager.LoadScene("GameOver");
                return;
            }
            // llamar al inventario de pokemons
            GMP.MostrarPokemon();
        }
    }

    public float CalcularEfectividad(Tipo TipoAtacante, Tipo TipoDefensor)
    {

        if (TipoAtacante == Tipo.HETERO && TipoDefensor == Tipo.CURIOSO
         || TipoAtacante == Tipo.CURIOSO && TipoDefensor == Tipo.SARASA
         || TipoAtacante == Tipo.SARASA && TipoDefensor == Tipo.HETERO
         || TipoAtacante == Tipo.LIMPIO && TipoDefensor == Tipo.HORNEADO
         || TipoAtacante == Tipo.HORNEADO && TipoDefensor == Tipo.ZARRAPASTROSO
         || TipoAtacante == Tipo.ZARRAPASTROSO && TipoDefensor == Tipo.LIMPIO)
        {
            return 2f;
        }
        else if (TipoAtacante == Tipo.HETERO && TipoDefensor == Tipo.SARASA
              || TipoAtacante == Tipo.CURIOSO && TipoDefensor == Tipo.HETERO
              || TipoAtacante == Tipo.SARASA && TipoDefensor == Tipo.CURIOSO
              || TipoAtacante == Tipo.LIMPIO && TipoDefensor == Tipo.ZARRAPASTROSO
              || TipoAtacante == Tipo.HORNEADO && TipoDefensor == Tipo.LIMPIO
              || TipoAtacante == Tipo.ZARRAPASTROSO && TipoDefensor == Tipo.HORNEADO)
        {
            return 0.5f;
        }
        else
        {
            return 1f;
        }

    }




    IEnumerator AnimAtaque()
    {
        float mov = 5f;
        yo.transform.position = new Vector3(yo.transform.position.x + mov, yo.transform.position.y, yo.transform.position.z);
        yield return new WaitForSecondsRealtime(3f);
        yo.transform.position = new Vector3(yo.transform.position.x - mov, yo.transform.position.y, yo.transform.position.z);
        HeClicadoEnBoton = false;
    }



    public void SetPokemons(ScriptableMokepon m_p, ScriptableMokepon e_p)
    {
        m_pokemon = m_p;
        enemy = e_p;
    }

    public void SetM_Pokemon(ScriptableMokepon m_p)
    {
        m_pokemon = m_p;
    }

    public void SetEnemy(ScriptableMokepon e_p)
    {
        enemy = e_p;
    }

    public void PonerAtaquesYPokemon()
    {
        ataques = AC.Getataques();
        m_pokemon = AC.GetPokemon();
    }


    public void SetTurno(bool t)
    {
        turno = t;
    }


    public ScriptableMokepon getEnemigo()
    {
        return enemy;
    }

}
