using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadInfo_Combate : MonoBehaviour
{

    // yo

    /*
    [SerializeField]
    List<GameObject> pokemons_disponibles = new List<GameObject>();
    */
    [SerializeField]
    List<ScriptableMokepon> mis_pokemons_disponibles; // desde un game manager se le pasara la lista e pokemons que tiene el jugador


    private ScriptableMokepon m_pokemonPrincipal;


    [SerializeField]
    GameObject m_nombre;

    [SerializeField]
    GameObject m_vida;

    [SerializeField]
    GameObject m_foto;




    // enemigo


    // al entrar en la escena hay que pasarle este piruko
    [SerializeField]
    ScriptableMokepon enemigo;

    [SerializeField]
    GameObject e_nombre;

    [SerializeField]
    GameObject e_foto;


    [SerializeField]
    GameObject Botones;




    //referencias
    [SerializeField]
    Turnos T;

    [SerializeField]
    HealthBarController pokemonVidaControler, enemigoVidaControler;


    private void Awake()
    {
        enemigo = Turnos.Instance.getEnemigo(); // es la copia (esta bien porque sino pillarias la fila de la bd (la fila de la bd seria el SO(Sistema Operativo)))
        print("llego al Awake del LoadInfo_Controller " + GeneralManager.Instance.getList()[1]);
        mis_pokemons_disponibles = GeneralManager.Instance.getList();
    }

    private void Start()
    {
        SetMyPokemon();
        LoadMyInfo();
        LoadEnemyInfo();
        
    }

    private void Update()
    {
        LoadMyInfo();
        Botones.GetComponent<Ataques_Controller>().setPokemonCombatiendo(m_pokemonPrincipal);

    }

 

    public void LoadMyInfo()
    {
        if (m_pokemonPrincipal)
        {
            m_foto.GetComponent<Image>().sprite = m_pokemonPrincipal.foto;
            m_nombre.GetComponent<TMPro.TextMeshProUGUI>().text = m_pokemonPrincipal.nom;
            m_vida.GetComponent<TMPro.TextMeshProUGUI>().text = m_pokemonPrincipal.hpActual + "/" + m_pokemonPrincipal.hpMax;
        }
    }


    public void LoadEnemyInfo()
    {
        if (enemigo)
        {
            e_foto.GetComponent<Image>().sprite = enemigo.foto;
            e_foto.transform.localScale = new Vector3(-1,1,1);

            e_nombre.GetComponent<TMPro.TextMeshProUGUI>().text = enemigo.nom;
            enemigoVidaControler.SetPokemonActula(enemigo);
        }
    }


    // todos los mis_pokemons_disponibles cambiar por pokemons_disponibles
    public void SetMyPokemon()
    {
        /*
        for (int i = 0; i< pokemons_disponibles.Count; i++)
        {
            if (pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().getInfo())
            {
                if (pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().getInfo().combatiendo)
                {
                    m_pokemonPrincipal = pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().getInfo();
                    T.SetM_Pokemon(m_pokemonPrincipal);
                    pokemonVidaControler.SetPokemonActula(m_pokemonPrincipal);
                    return;
                }
            }
            else
            {
                print("no tienes un pokemon en el hueco " + i);
            }
        }

       

        m_pokemonPrincipal = pokemons_disponibles[0].GetComponent<LoadInfo_Pokemons>().getInfo();
        T.SetM_Pokemon(m_pokemonPrincipal);
        pokemonVidaControler.SetPokemonActula(m_pokemonPrincipal);

        pokemons_disponibles[0].GetComponent<LoadInfo_Pokemons>().getInfo().combatiendo = true;
        */

        for (int i = 0; i < mis_pokemons_disponibles.Count; i++)
        {
            if (mis_pokemons_disponibles[i])
            {
                if (mis_pokemons_disponibles[i].combatiendo)
                {
                    m_pokemonPrincipal = mis_pokemons_disponibles[i];
                    T.SetM_Pokemon(m_pokemonPrincipal);
                    m_pokemonPrincipal.combatiendo = true;
                    pokemonVidaControler.SetPokemonActula(m_pokemonPrincipal);
                    return;
                } 
            }
        }

        m_pokemonPrincipal = mis_pokemons_disponibles[0];
        T.SetM_Pokemon(m_pokemonPrincipal);
        m_pokemonPrincipal.combatiendo = true;
        pokemonVidaControler.SetPokemonActula(m_pokemonPrincipal);

        LoadMyInfo();
    }



    /* // cero referencias
    public void SetInfoPokemons()
    {
        for (int i = 0; i < mis_pokemons_disponibles.Count; i++)
        {
            pokemons_disponibles[i].GetComponent<LoadInfo_Pokemons>().setInfo(mis_pokemons_disponibles[i]);
        }
    }
    */
}
