using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameButton : MonoBehaviour
{
    public void SaveGame()
    {
        print("Saving game");
        GeneralManager.Instance.Save();
    }

    public void LoadGame()
    {
        print("Loading game");
        GeneralManager.Instance.Load();
    }
}
