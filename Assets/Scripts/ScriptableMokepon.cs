using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "Mokepon", menuName = "ScriptableObjects/Mokepon")]
[Serializable]
public class ScriptableMokepon : ScriptableObject
{
    public int id;
    public string nom;
    public List<Movimiento> movimientos;
    public int hpActual;
    public int hpMax;
    public int atk;
    public int def;
    public int vel;
    public int nivel;
    public int exp;
    public Equipo equipo;
    public Tipo tipoSexualidade;
    public Tipo tipoActividadSesua;
    public bool vivo = true;
    public Sprite foto;
    public bool combatiendo;
}
