using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mochila : MonoBehaviour
{

    [SerializeField]
    List<GameObject> mochila = new List<GameObject>();

    [SerializeField]
    List<Equipo> m_Items = new List<Equipo>();


    List<Equipo> itemsPj;

    // yo mismo // por si luego me hace falta
    [SerializeField]
    GameObject invetario;

    [SerializeField]
    private GameObject selector;

    [SerializeField]
    private GameObject Opciones;

    [SerializeField]
    private GameObject OpcionesDos;


    [SerializeField]
    private ScriptableMokepon PokemonObjetivo;



    [SerializeField]
    private int ID;





    private void Awake()
    {
        itemsPj = GeneralManager.Instance.inventario();
        loadnInforPonerInventario();
        loadInfo();
    }

    private void loadnInforPonerInventario()
    {
        for (int i = 0; i < itemsPj.Count; i++)
        {
            if (itemsPj[i])
            {
                m_Items[i] = itemsPj[i];
            }
        }
        for (int i = 0; i < mochila.Count; i++)
        {
            if (m_Items[i]) {
                mochila[i].GetComponent<Image>().sprite = m_Items[i].foto;
                mochila[i].GetComponent<Image>().enabled = true;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        Surfear();
    }


    bool PoderMoverse = true;
    public void Surfear()
    {

        if (PoderMoverse) {
            if (Input.GetKeyDown(KeyCode.D) && ID < mochila.Count - 1)
            {
                ID++;
            }
            if (Input.GetKeyDown(KeyCode.A) && ID > 0)
            {
                ID--;
            }

            if (Input.GetKeyDown(KeyCode.W) && ID > 3)
            {
                ID -= 4;
            }
            if (Input.GetKeyDown(KeyCode.S) && ID < 8)
            {
                ID += 4;
            }

            selector.transform.position = mochila[ID].transform.position;
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            
            if (PoderMoverse)
            {
                if (m_Items[ID] == null)
                {
                    print("No hay ningun objeto seleccionado");

                    QuitarOpciones();
                }
                else if (m_Items[ID].funcion == 0)
                {
                    PoderMoverse = false;
                    OpcionesDos.SetActive(true);
                    OpcionesDos.transform.position = mochila[ID].transform.position;
                }
                else if(m_Items[ID].funcion == 1)
                {
                    PoderMoverse = false;
                    Opciones.SetActive(true);
                    Opciones.transform.position = mochila[ID].transform.position;
                }


            }
            else
            {
                QuitarOpciones();
            }

        }
        
    }



    public void QuitarOpciones()
    {
        Opciones.SetActive(false);
        OpcionesDos.SetActive(false);
        PoderMoverse = true;


         // lo quito para que no se quede hay colgando y tal
    }

    //hay que adaptarlo
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Item"))
        {
            for (int i = 0; i < mochila.Count; i++)
            {
                if (mochila[i].GetComponent<Image>().enabled == false)
                {
                    mochila[i].GetComponent<Image>().enabled = true;
                    mochila[i].GetComponent<Image>().sprite = collision.GetComponent<SpriteRenderer>().sprite;
                    // ponerle el scriptable del objeto que a tocado, como no se que es lo que toca exactamente no lo escrivo y tal
                    //m_Items[i] = collision.GetComponent<ObjectoItem>().getScriptable();
                    break;
                }
            }
        }
    }




    public void añadirObjectoInventario(Equipo item)
    {
        for (int i = 0; i < mochila.Count; i++)
        {
            // donde encunetra un weko
            if (mochila[i].GetComponent<Image>().enabled == false)
            {

                mochila[i].GetComponent<Image>().enabled = true;
                mochila[i].GetComponent<Image>().sprite = item.foto;
                m_Items[i] = item;
                loadInfo();
                
                break;
            }
        }
    }


    public void loadInfo()
    {
        for (int i = 0; i < mochila.Count; i++)
        {
            if (mochila[i].GetComponent<Image>().enabled == true)
            {
                mochila[i].GetComponent<Image>().enabled = false;
                mochila[i].GetComponent<Image>().enabled = true;
            }
        }
    }



    public void SetPokemonAfectado(ScriptableMokepon ScPokemon)
    {
        PokemonObjetivo = ScPokemon;
    }




    public void DarObjectoAlPokemon()
    {
        print("Clicko en DAR");


        if (m_Items[ID] != null)
        {
            print("Utilizo DAR");

            mochila[ID].GetComponent<Image>().enabled = false;
            PokemonObjetivo.equipo = m_Items[ID]; // se lo doy al pokemon

            /*
            if (m_Items[ID].statSumada == BuffoStat.ATK)
            {
                PokemonObjetivo.atk = PokemonObjetivo.atk + m_Items[ID].sumaStat;
            }
            else if (m_Items[ID].statSumada == BuffoStat.ATKSP)
            {
                PokemonObjetivo.atk = PokemonObjetivo.atk + (m_Items[ID].sumaStat*2);
            }
            else if (m_Items[ID].statSumada == BuffoStat.DEF)
            {
                PokemonObjetivo.def = PokemonObjetivo.def + m_Items[ID].sumaStat;
            }
            else if (m_Items[ID].statSumada == BuffoStat.DEFSP)
            {
                PokemonObjetivo.def = PokemonObjetivo.def + (m_Items[ID].sumaStat * 2);
            }
            else if (m_Items[ID].statSumada == BuffoStat.VEL)
            {
                PokemonObjetivo.vel = PokemonObjetivo.vel + m_Items[ID].sumaStat;
            }
            */

            m_Items.Remove(m_Items[ID]); // lo quito de la lista
            loadInfo();

            PokemonObjetivo = null; // lo quito y tal
        }

    }



    public void UsarObjecto()
    {
        print("Clicko en USAR");
        if (m_Items != null)
        {
            mochila[ID].GetComponent<Image>().enabled = false;

            if (m_Items[ID].statSumada == BuffoStat.HP)
            {
                if (PokemonObjetivo.hpActual + m_Items[ID].curacion > PokemonObjetivo.hpMax)
                {
                    PokemonObjetivo.hpActual = PokemonObjetivo.hpMax;
                }
                else
                {
                    PokemonObjetivo.hpActual += m_Items[ID].curacion;
                }

                m_Items[ID] = null;
                PokemonObjetivo = null;
            }
        }
    }



}




