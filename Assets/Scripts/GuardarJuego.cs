using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GuardarJuego : MonoBehaviour
{
    [SerializeField]
    private PJ pj;
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private GuardaPartida partida;
    [SerializeField]
    private Pokedex pokedex;
    [SerializeField]
    private CompendioObjetos compendio;


    private void Awake()
    {
        //DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            Save();
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            Load();
        }
    }

    void Save()
    {
        partida.posPJ = this.transform.position;
        for(int i = 0; i < pj.nMokepons; i++)
        {
            print(i+" "+pj.equipo[i].nom);
            partida.equipoPoguemons[i] = pj.equipo[i].id;
        }
        print(pj.inventario.Count);
        for(int i = 0; i < pj.inventario.Count; i++)
        {
            partida.inventario[i] = pj.inventario[i].nombre;
            partida.cantidadObjeto[i] = pj.inventario[i].cantidad;
        }
        string jsonStr = JsonUtility.ToJson(partida);
        print(jsonStr);
        File.WriteAllText("savegame.json", jsonStr);
        print("Game Saved");
    }

    void Load()
    {

        string jsonStr = File.ReadAllText("savegame.json");

        JsonUtility.FromJsonOverwrite(jsonStr, partida);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        target.transform.position = partida.posPJ;
        pj.transform.position = partida.posPJ;

        while (pj.equipo.Count > 0)
        {
            pj.equipo.RemoveAt(0);
        }

        while(pj.inventario.Count > 0)
        {
            pj.inventario.RemoveAt(0);
        }
        
        for (int i = 0; i < partida.equipoPoguemons.Length; i++)
        {
            for(int j = 0; j < pokedex.pokedex.Count; j++)
            {
                if(partida.equipoPoguemons[i] == pokedex.pokedex[j].id)
                {
                    pj.equipo.Add(pokedex.pokedex[j]);
                }
            }
        }

        pj.nMokepons = pj.equipo.Count;

        while(pj.equipo.Count < 6)
        {
            pj.equipo.Add(null);
        }

        for (int i = 0; i < partida.inventario.Length; i++)
        {
            for (int j = 0; j < compendio.compendio.Count; j++)
            {
                if (partida.inventario[i] == compendio.compendio[j].nombre)
                {
                    pj.inventario.Add(compendio.compendio[j]);
                    pj.inventario[pj.inventario.Count - 1].cantidad = partida.cantidadObjeto[i];
                }
            }
        }

        print("Game Loaded");
    }
}

