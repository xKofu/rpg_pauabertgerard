using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoaderManager : MonoBehaviour
{
    public Animator transicion;

    [SerializeField]
    private GuardaPartida estadoAlCambiarEscena;

    [SerializeField]
    private GameEvent Bosque;

    [SerializeField]
    private GameEvent Cueva;

    private void Start()
    {
        if(SceneManager.GetActiveScene().name == "EscenaCombate")
        {
            //GameObject.Find("SaveLoad").SetActive(false);
        }
    }

    public void acabarCombate()
    {
        LoadNextLevel(estadoAlCambiarEscena.nombreEscena);
    }

    public void ReturnLevel()
    {
        if (estadoAlCambiarEscena.nombreEscena == "Gerard")
        {
            Bosque.Raise();
        } else if (estadoAlCambiarEscena.nombreEscena == "Cueva")
        {
            Cueva.Raise();
        }
    }

    public void LoadNextLevel(string nombreEscena)
    {
        
        //if (nombreEscena != estadoAlCambiarEscena.nombreEscena && nombreEscena != "EscenaCombate")
        //    nombreEscena = estadoAlCambiarEscena.nombreEscena;

        if (nombreEscena == "Cueva" && estadoAlCambiarEscena.nombreEscena == "Gerard")
        {
            estadoAlCambiarEscena.posPJ = GameObject.Find("CambioCueva").transform.position;
        }

        if (nombreEscena == "Gerard" && estadoAlCambiarEscena.nombreEscena == "Cueva")
        {
            estadoAlCambiarEscena.posPJ = GameObject.Find("Bosque").transform.position;
        }

        if (nombreEscena == "EscenaCombate")
        {
            GeneralManager.Instance.OnEntrarCombate();
        }

        print(nombreEscena);

        print("B");
        StartCoroutine(LoadLevel(nombreEscena));
    }

    IEnumerator LoadLevel(string nombreEscena)
    {
        print("C");

        transicion.SetTrigger("Start");

        yield return new WaitForSeconds(1);

        SceneManager.LoadScene(nombreEscena);
    }
}
