using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarController : MonoBehaviour
{

    [SerializeField]
    private Image HealthBar;

    public float vidaTotal;

    [SerializeField]
    private ScriptableMokepon pokemonActual;


    private void Awake()
    {
        
    }

    public void Fill()
    {
        HealthBar.fillAmount = pokemonActual.hpActual / vidaTotal;
        GetComponent<AudioSource>().Play();
    }

    public void FillG()
    {
        HealthBar.fillAmount = pokemonActual.hpActual / vidaTotal;
        print("llego " + pokemonActual.nom);
        
    }


    public void SetPokemonActula(ScriptableMokepon SPA)
    {
        pokemonActual = SPA;
        loadInfo();
    }

    public void loadInfo()
    {
        vidaTotal = pokemonActual.hpMax;

        HealthBar.type = Image.Type.Filled;
        HealthBar.fillMethod = Image.FillMethod.Horizontal;
        HealthBar.fillAmount = pokemonActual.hpActual / vidaTotal;
    }

    IEnumerator BajarVidaProgresiva(float ReceivedDmg)
    {
        while(pokemonActual.hpActual != ReceivedDmg)
        {
            HealthBar.fillAmount = pokemonActual.hpActual / vidaTotal;
            pokemonActual.hpActual--;
            yield return new WaitForSeconds(.1f);
        }
    }
}
