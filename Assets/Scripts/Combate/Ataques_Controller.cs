using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ataques_Controller : MonoBehaviour
{

    [SerializeField]
    private List<GameObject> ataques_texto = new List<GameObject>();


    [SerializeField]
    private List<Movimiento> ataques = new List<Movimiento>();



    //private ScriptableMokepon m_pokemonPrincipal;

    [SerializeField]
    private ScriptableMokepon m_scriptable_pokemon;





    private void OnEnable()
    {
        loadInfoAtaques();
    }

    public void setPokemonCombatiendo(ScriptableMokepon ScriPokemon)
    {
        m_scriptable_pokemon = ScriPokemon;
    }


    public void loadInfoAtaques()
    {
        if(!m_scriptable_pokemon)
        {
            return;
        }

        for (int i = 0; i < ataques.Count; i++)
        {
            if (m_scriptable_pokemon.movimientos[i])
            {
                ataques[i] = m_scriptable_pokemon.movimientos[i];
                
            }
            else
            {
                //print("No tengo ataque");
                ataques[i] = null; // bromisqui (poner salpicadura) // si se hace se solcionaria solo lo de que alguien no tenga ataques y utilizaria ese para pasar el truno
            }
        }

        for (int i = 0; i < ataques_texto.Count; i++)
        {
            if (ataques[i])
            {
                ataques_texto[i].GetComponent<TMPro.TextMeshProUGUI>().text = ataques[i].nombre + " " + ataques[i].usos + "/" + ataques[i].usosTotales;
            }
            else
            {
                ataques_texto[i].GetComponent<TMPro.TextMeshProUGUI>().text = "Ataque's " + i;
            }
        }
        
    }



    public List<Movimiento> Getataques()
    {
        return ataques;
    }


    public ScriptableMokepon GetPokemon()
    {
        return m_scriptable_pokemon;
    }
}
